package com.journal.ejournal

import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.ext.junit.runners.AndroidJUnit4

import org.junit.Test
import org.junit.runner.RunWith

import org.junit.Assert.*

/**
 * Instrumented com.journal.ejournal.test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class journalInstrumentedTest {
    @Test
    fun useAppContext() {
        // Context of the app under com.journal.ejournal.test.
        val appContext = InstrumentationRegistry.getInstrumentation().targetContext
        assertEquals("com.journal.ejournal", appContext.packageName)
    }
}