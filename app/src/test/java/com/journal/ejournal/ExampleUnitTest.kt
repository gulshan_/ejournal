package com.journal.ejournal

import org.junit.Test

import org.junit.Assert.*

/**
 * journal local unit com.journal.ejournal.test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class journalUnitTest {
    @Test
    fun addition_isCorrect() {
        assertEquals(4, 2 + 2)
    }
}