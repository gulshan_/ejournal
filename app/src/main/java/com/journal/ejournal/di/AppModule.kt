package com.journal.ejournal.di

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import com.journal.ejournal.api.RetrofitApi
import com.journal.ejournal.repo.AuthRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton


@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Provides
    @Singleton
    fun provideContext(app: Application): Context = app.applicationContext

    @Provides
    fun provideSharedPreferences(context: Context): SharedPreferences =
        context.getSharedPreferences("ejournal", Context.MODE_PRIVATE)

    @Provides
    fun provideAuthRepository(apiService: RetrofitApi) = AuthRepository(apiService)
}