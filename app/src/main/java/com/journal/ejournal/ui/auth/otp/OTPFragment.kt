package com.journal.ejournal.ui.auth.otp

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import com.journal.ejournal.R
import com.journal.ejournal.databinding.OtpFragmentBinding

class OTPFragment : Fragment(R.layout.otp_fragment) {

    private var fragmentBinding: OtpFragmentBinding? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val binding = OtpFragmentBinding.bind(view)
        fragmentBinding = binding

    }
}