package com.journal.ejournal.ui.auth.update

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import com.journal.ejournal.R
import com.journal.ejournal.databinding.ForgotPasswordFragmentBinding

class ForgotPasswordFragment : Fragment(R.layout.forgot_password_fragment) {

    private var fragmentFinding: ForgotPasswordFragmentBinding? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val binding = ForgotPasswordFragmentBinding.bind(view)
        fragmentFinding = binding
    }
}