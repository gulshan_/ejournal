package com.journal.ejournal.ui.auth.fin

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.journal.ejournal.R
import com.journal.ejournal.databinding.CheckFinFragmentBinding
import com.journal.ejournal.ui.dialogs.Dialog
import com.journal.ejournal.util.Status
import com.journal.ejournal.util.isInvisible
import com.journal.ejournal.util.isVisible
import com.journal.ejournal.util.toStr
import dagger.hilt.android.AndroidEntryPoint

private const val TAG = "CheckFinFragment"

@AndroidEntryPoint
class CheckFinFragment : Fragment(R.layout.check_fin_fragment) {

    private val viewModel: FinViewModel by viewModels()
    private var fragmentBinding: CheckFinFragmentBinding? = null
    private val binding get() = fragmentBinding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        fragmentBinding = CheckFinFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.finInfo.setOnClickListener {
            findNavController().navigate(CheckFinFragmentDirections.actionCheckFinFragmentToFinInfoFragment())
        }
        binding.checkFinBtn.setOnClickListener {
            viewModel.checkFin(binding.finTxt.toStr()).observe(
                viewLifecycleOwner
            ) {
                it.let { response ->
                    when (response.status) {
                        Status.LOADING -> {
                            binding.progressBar.isVisible()
                        }
                        Status.success -> {
                            binding.progressBar.isInvisible()
                            findNavController().navigate(CheckFinFragmentDirections.actionCheckFinFragmentToCheckMobileNumberFragment())
                            Log.d(TAG, "success$response")
                        }
                        Status.ERROR -> {
                            binding.progressBar.isInvisible()
                            response.message?.let { msg ->
                                Dialog(
                                    requireContext(), R.string.error, R.string.enter,
                                    msg
                                ).showDialog()
                            }
                        }
                    }
                }
            }
        }
    }


    override fun onDestroyView() {
        super.onDestroyView()
        fragmentBinding = null
    }
}