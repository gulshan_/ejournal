package com.journal.ejournal.ui.dialogs

import android.content.Context
import androidx.appcompat.app.AlertDialog
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.journal.ejournal.R


class Dialog(var context: Context, var title: Int, var btn: Int, var message: String) {
    fun showDialog() {
        val builder: AlertDialog.Builder = MaterialAlertDialogBuilder(
            context, R.style.MaterialAlertDialog_rounded
        )
        builder.setMessage(message)
            .setTitle(title)
            .setPositiveButton(btn) { _, _ -> }.show()
    }
}

internal interface ClickPositive {
    fun onClick()
}