package com.journal.ejournal.ui.auth.fin

import android.content.SharedPreferences
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.journal.ejournal.repo.AuthRepository
import com.journal.ejournal.util.Resource
import com.journal.ejournal.util.getErrorMessage
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import retrofit2.HttpException
import javax.inject.Inject

@HiltViewModel
class FinViewModel @Inject constructor(
    private val repository: AuthRepository,
    private val prefs: SharedPreferences
) : ViewModel() {

    fun checkFin(finCode: String) = liveData(Dispatchers.IO) {
        emit(Resource.loading(null))
        try {
            emit(Resource.success(data = repository.checkFin(finCode)))
        } catch (exception: HttpException) {
            emit(Resource.error(msg = exception.getErrorMessage() ?: "Error Occurred", data = null))
        }
    }
}