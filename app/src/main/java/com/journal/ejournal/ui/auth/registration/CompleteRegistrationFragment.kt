package com.journal.ejournal.ui.auth.registration

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import com.journal.ejournal.R
import com.journal.ejournal.databinding.RegistrationFragmentBinding

class CompleteRegistrationFragment : Fragment(R.layout.registration_fragment) {

    private var fragmentBinding: RegistrationFragmentBinding? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val binding = RegistrationFragmentBinding.bind(view)
        binding.signupBtn.setOnClickListener {
            //move to select gender
        }
    }
}