package com.journal.ejournal.ui.auth.login

import android.app.AlertDialog
import android.content.res.Configuration
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.journal.ejournal.R
import com.journal.ejournal.databinding.LoginFragmentBinding
import com.journal.ejournal.util.*
import dagger.hilt.android.AndroidEntryPoint
import java.util.*

private const val TAG = "LoginFragment"

@AndroidEntryPoint
class LoginFragment : Fragment(R.layout.login_fragment) {
    private val viewModel: LoginViewModel by viewModels()
    private var fragmentBinding: LoginFragmentBinding? = null
    private val binding get() = fragmentBinding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        fragmentBinding = LoginFragmentBinding.inflate(inflater, container, false)
        Log.d(TAG, "onCreateView: " + viewModel.getPref().getString("lang", "nothing"))

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initLanguage()
        binding.registration.setOnClickListener {
            findNavController().navigate(LoginFragmentDirections.loginToCheckFin())
        }
        binding.loginBtn.setOnClickListener {
            if (checkInputs(binding.finEditText, binding.passwordEditText)) {
                viewModel.login(binding.finEditText.toStr(), binding.passwordEditText.toStr())
                    .observe(viewLifecycleOwner) {
                        it.let { response ->
                            when (response.status) {
                                Status.LOADING -> {
                                    binding.progressBar.isVisible()
//                                    Log.d(TAG, "loading ${response.data}")
                                }
                                Status.success -> {
                                    binding.progressBar.isInvisible()
                                    findNavController().navigate(LoginFragmentDirections.loginFragmentToMain())
//                                    Log.d(TAG, "success$response")
                                    response.data
                                }
                                Status.ERROR -> {
                                    toast("${response.message}")
                                    binding.progressBar.isInvisible()
//                                    Log.d(TAG, "error $response")
                                }
                            }
                        }
                    }
            } else {
                toast("Please fill empty data")
            }
        }
        binding.passwordForgotTxt.setOnClickListener {
            findNavController().navigate(LoginFragmentDirections.loginToForgotPassword())
        }
        binding.languageImage.setOnClickListener {
            val builder = AlertDialog.Builder(requireContext(), R.style.MaterialAlertDialog_rounded)
            builder.setTitle(R.string.please_language)
                .setItems(R.array.languages) { dialog, menuItem ->
                    var lang = "en"
                    if (menuItem == 0) {
                        setLocale("az")
                        viewModel.getPref().edit().putString("lang", "az").apply()
                    } else if (menuItem == 1) {
                        setLocale("ru")
                        viewModel.getPref().edit().putString("lang", "ru").apply()
                    } else {
                        setLocale("en")
                        viewModel.getPref().edit().putString("lang", "en").apply()
                    }
                    findNavController().navigate(R.id.loginFragment)
                }
            builder.create().show()
        }
    }

    fun setLocale(localeCode: String?) {
        val locale = Locale(localeCode)
        Locale.setDefault(locale)
        val config = Configuration()
        config.locale = locale
        this.resources.updateConfiguration(config, this.resources.displayMetrics)
    }

    fun initLanguage() {
        val language = viewModel.getPref().getString("lang", "nothing")

        if (language != resources.configuration.locale.language) {
            setLocale(language)
            findNavController().navigate(R.id.loginFragment)
        }

        if (language == "az") {
            binding.languageTxt.text = "AZ"
        } else if (language == "ru") {
            binding.languageTxt.text = "RU"
        } else {
            binding.languageTxt.text = "EN"
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        fragmentBinding = null
    }

}