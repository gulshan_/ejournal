package com.journal.ejournal.ui

import android.os.Bundle
import android.view.View
import androidx.core.view.WindowCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.WindowInsetsControllerCompat
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.journal.ejournal.BuildConfig
import com.journal.ejournal.R
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class SplashFragment : Fragment(R.layout.splash_fragment) {
    override fun onResume() {
        super.onResume()
        hideStatusBar()
        CoroutineScope(Dispatchers.Main).launch {
            delay(3000)
            findNavController().navigate(SplashFragmentDirections.actionSplashFragmentToLoginFragment())
        }

    }

    private fun hideStatusBar() {
        WindowCompat.setDecorFitsSystemWindows(requireActivity().window, false)
        view?.let {
            WindowInsetsControllerCompat(requireActivity().window, it).let { i ->
                BuildConfig.VERSION_NAME

                i.hide(WindowInsetsCompat.Type.systemBars())
                i.systemBarsBehavior =
                    WindowInsetsControllerCompat.BEHAVIOR_SHOW_TRANSIENT_BARS_BY_SWIPE
            }
        }
    }
}