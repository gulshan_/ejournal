package com.journal.ejournal.ui.auth.mobile

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.journal.ejournal.R
import com.journal.ejournal.databinding.PhoneNumberFragmentBinding

class CheckMobileNumberFragment : Fragment(R.layout.phone_number_fragment) {

    private var fragmentBinding: PhoneNumberFragmentBinding? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val binding = PhoneNumberFragmentBinding.bind(view)
        fragmentBinding = binding
        binding.continueBtn.setOnClickListener {
            findNavController().navigate(CheckMobileNumberFragmentDirections.actionCheckMobileNumberFragmentToOTPFragment())
        }

    }

    override fun onDestroyView() {
        super.onDestroyView()
    }
}