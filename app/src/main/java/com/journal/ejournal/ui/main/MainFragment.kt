package com.journal.ejournal.ui.main

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import com.journal.ejournal.R
import com.journal.ejournal.databinding.MainFragmentBinding
import com.journal.ejournal.databinding.NavHeaderLayoutBinding

class MainFragment : Fragment(R.layout.main_fragment) {

    private var fragmentBinding: MainFragmentBinding? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val binding = MainFragmentBinding.bind(view)
        fragmentBinding = binding
        setNavMenu()
    }

    private fun setNavMenu() {
//        val _bind: NavHeaderLayoutBinding = DataBindingUtil.inflate(layoutInflater,
//            R.layout.nav_header_layout,
//            binding.navView,
//            false)


    }

}