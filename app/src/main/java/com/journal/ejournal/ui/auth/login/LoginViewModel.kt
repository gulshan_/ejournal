package com.journal.ejournal.ui.auth.login

import android.content.SharedPreferences
import android.util.Patterns
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.journal.ejournal.repo.AuthRepository
import com.journal.ejournal.util.Resource
import com.journal.ejournal.util.getErrorMessage
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import retrofit2.HttpException
import javax.inject.Inject

@HiltViewModel
class LoginViewModel @Inject constructor(
    private val repository: AuthRepository,
    private val pref: SharedPreferences,
) : ViewModel() {

    fun login(username: String, password: String) = liveData(Dispatchers.IO) {
        emit(Resource.loading(null))
        try {
            if (Patterns.PHONE.matcher(username.trim { it <= ' ' }).matches()) {
                emit(Resource.success(data = repository.loginWithNumber(username, password)))
            } else if (Patterns.EMAIL_ADDRESS.matcher(username.trim { it <= ' ' }).matches()) {
                emit(Resource.success(data = repository.loginWithMail(username, password)))
            } else {
                emit(Resource.success(data = repository.loginWithFin(username, password)))
            }
        } catch (exception: HttpException) {
            emit(Resource.error(msg = exception.getErrorMessage() ?: "Error Occurred!",
                data = null))
        }
    }

    fun getPref(): SharedPreferences {
        return pref
    }

}