package com.journal.ejournal.repo

import com.journal.ejournal.api.RetrofitApi
import javax.inject.Inject

class AuthRepository @Inject constructor(
    private val retrofitApi: RetrofitApi
) {
    suspend fun loginWithFin(username: String, password: String) =
        retrofitApi.loginByFin(username, password)

    suspend fun loginWithNumber(phoneNumber: String, password: String) =
        retrofitApi.loginByPhone(phoneNumber, password)

    suspend fun loginWithMail(mail: String, password: String) =
        retrofitApi.loginByMail(mail, password)

    suspend fun checkFin(finCode: String) = retrofitApi.checkFin(finCode)
}