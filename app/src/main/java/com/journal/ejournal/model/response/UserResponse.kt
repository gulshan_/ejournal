package com.journal.ejournal.model.response

class UserResponse(
    val status: String?,
    val data: User?
)