package com.journal.ejournal.model.response

class Data(
    val documentNumber: String?,
    val documentType: String?,
    val finCode: String?,
    val firstName: String = "",
    val lastName: String = "",
    val patronymic: String = "",
    val birthDate: String?,
    val idSerialNumber: String?,
    val citizenship: String?,
    val address: String?,
    val image: String?
)

