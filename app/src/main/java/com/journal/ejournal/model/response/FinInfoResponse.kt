package com.journal.ejournal.model.response

class FinInfoResponse(
    val status: String,
    val data: Data,
)
