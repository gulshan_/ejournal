package com.journal.ejournal.model.response

data class User(
    val id: Int,
    val username: String,
    val firstname: String,
    val lastname: String,
    val fullname: String,
    val fin: String,
    val email: String,
    val phone: String,
    val token: String,
    val image: String
)
