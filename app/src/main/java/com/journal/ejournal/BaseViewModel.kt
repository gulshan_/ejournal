package com.journal.ejournal

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

open class BaseViewModel : ViewModel() {
    protected val _error = MutableLiveData<String>()
    protected val _loading = MutableLiveData<String>()

}