package com.journal.ejournal.api

import com.journal.ejournal.model.response.FinInfoResponse
import com.journal.ejournal.model.response.UserResponse
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.Headers
import retrofit2.http.POST

interface RetrofitApi {
    @FormUrlEncoded
    @Headers("Accept:application/json")
    @POST("/api/v1/auth/login")
    suspend fun loginByFin(
        @Field("username") username: String?,
        @Field("password") password: String?
    ): UserResponse

    @FormUrlEncoded
    @Headers("Accept:application/json")
    @POST("/api/v1/auth/login")
    suspend fun loginByPhone(
        @Field("phone") phone: String?,
        @Field("password") password: String?
    ): UserResponse

    @FormUrlEncoded
    @Headers("Accept:application/json")
    @POST("/api/v1/auth/login")
    suspend fun loginByMail(
        @Field("email") email: String?,
        @Field("password") password: String?
    ): UserResponse

    @FormUrlEncoded
    @POST("/api/v1/auth/check-fin")
    suspend fun checkFin(
        @Field("fin") fin: String
    ): FinInfoResponse
}