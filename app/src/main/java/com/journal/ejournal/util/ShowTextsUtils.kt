package com.journal.ejournal.util

import android.widget.Toast
import androidx.fragment.app.Fragment
import org.json.JSONObject
import retrofit2.HttpException

fun Fragment.toast(msg: String) {
    Toast.makeText(this.requireContext(), msg, Toast.LENGTH_SHORT).show()
}
fun HttpException.getErrorMessage():String{
   return JSONObject(this.response()?.errorBody()?.string()).getJSONArray("errors")[0].toString()
}