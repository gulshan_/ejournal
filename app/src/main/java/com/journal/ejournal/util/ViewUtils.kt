package com.journal.ejournal.util

import android.view.View

fun View.isVisible() {
    this.visibility = View.VISIBLE
}

fun View.isInvisible() {
    this.visibility = View.INVISIBLE
}

fun View.isGone() {
    this.visibility = View.GONE
}