package com.journal.ejournal.util

import android.widget.EditText

fun EditText.toStr(): String {
    return this.text.toString()
}

fun EditText.isEmptyOrNull(): Boolean {
    return this.text.isNullOrEmpty()
}

fun checkInputs(vararg inputs: EditText): Boolean {
    inputs.forEach {
        if (it.text.isEmpty()) {
            return false
        }
    }
    return true
}