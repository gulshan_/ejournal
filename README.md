# Introduction
This is a app that enables parents to receive timely
updates (daily marks ,exams marks etc), to track child’s progress.

# Screenshots
![Home.svg](/uploads/eebc17d4248e009cfb8aa9929bf2fffc/Home.svg)
![Log_in.svg](/uploads/0b35a10fb5bd6da896cc99a0241b8bdb/Log_in.svg)
![fin_kod.svg](/uploads/bacec6cb17f607ef909d637474dff53d/fin_kod.svg)
![mobile](/uploads/9c85ff00933f90a7a5978f2508b819e1/mobile.jpg)
![00_59](/uploads/da2da8aa2c4f762636f9c5e302f6cc9e/00_59.jpg)
![male_selected.svg](/uploads/10019ba2e44e7e99e667eaa708e67918/male_selected.svg)
![shagird.svg](/uploads/6fc9835180d57a03b6894a4f9b60c2ab/shagird.svg)
![Anar_Huseynov_Profile](/uploads/eb543c7d4641f997c95cac235a198c25/Anar_Huseynov_Profile.jpg)
![Gundelik.svg](/uploads/838f1880e8eb912663c49e73ef0e3bfd/Gundelik.svg)
 
# Architecture
MVVM 

# Libraries and Technologies

Retrofit: For making network requests and interacting with RESTful APIs.

Glide: For efficient image loading and caching.

Coroutines: For asynchronous and non-blocking programming, providing a more fluent and structured approach compared to traditional callbacks.

Hilt: For dependency injection, enabling easier testing and modularization of code.

# Contact
If you have any questions or feedback regarding the project architecture, feel free to contact us at gulshan.rahimova.dev@gmail.com.

